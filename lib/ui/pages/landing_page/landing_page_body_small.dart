import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_demo/ui/pages/landing_page/landing_page_header.dart';
import 'package:responsive_demo/ui/pages/landing_page/tab_toggle.dart';
import 'package:responsive_demo/ui/widgets/bottom_wave_gradient_container.dart';

class LandingPageBodySmall extends StatefulWidget {
  const LandingPageBodySmall({Key? key}) : super(key: key);

  @override
  State<LandingPageBodySmall> createState() => _LandingPageBodySmallState();
}

class _LandingPageBodySmallState extends State<LandingPageBodySmall> {
  int _selectedTabIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BottomWaveGradientContainer(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(height: 16),
              const LandingPageHeader(),
              SvgPicture.asset(
                'assets/svgs/agreement.svg',
                fit: BoxFit.fitWidth,
              ),
            ],
          ),
        ),
        const SizedBox(height: 30),
        TabToggle(
          selectedIndex: _selectedTabIndex,
          onSelected: (i) => setState(() => _selectedTabIndex = i),
        ),
        const SizedBox(height: 30),
      ],
    );
  }
}
