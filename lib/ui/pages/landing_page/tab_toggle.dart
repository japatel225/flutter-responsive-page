import 'package:flutter/material.dart';
import 'package:responsive_demo/ui/colors.dart';
import 'package:responsive_demo/ui/utils.dart';

class TabToggle extends StatelessWidget {
  final int selectedIndex;
  final void Function(int) onSelected;

  const TabToggle({
    Key? key,
    required this.selectedIndex,
    required this.onSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ToggleButtons(
      direction: Axis.horizontal,
      onPressed: onSelected,
      borderRadius: BorderRadius.circular(12),
      selectedColor: Colors.white,
      fillColor: AppColors.c81E6D9,
      color: AppColors.c319795,
      selectedBorderColor: AppColors.c81E6D9,
      constraints: BoxConstraints(
        minHeight: 40.0,
        minWidth: context.isLargeScreen ? 160 : 112,
      ),
      isSelected: List.generate(3, (i) => i == selectedIndex),
      children: const [
        Text('Arbeitnehmer'),
        Text('Arbeitgeber'),
        Text('Temporärbüro'),
      ],
    );
  }
}
