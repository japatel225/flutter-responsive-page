import 'package:flutter/material.dart';
import 'package:responsive_demo/ui/colors.dart';
import 'package:responsive_demo/ui/pages/landing_page/landing_page_body_large.dart';
import 'package:responsive_demo/ui/pages/landing_page/landing_page_body_small.dart';
import 'package:responsive_demo/ui/utils.dart';
import 'package:responsive_demo/ui/text_styles.dart';
import 'package:responsive_demo/ui/widgets/gradient_button.dart';

class LandingScreen extends StatelessWidget {
  const LandingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(12)),
        ),
        actions: [
          TextButton(
            onPressed: () {},
            child: Text('Login', style: 14.semiBold(color: AppColors.c319795)),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: context.isLargeScreen
            ? const LandingPageBodyLarge()
            : const LandingPageBodySmall(),
      ),
      bottomNavigationBar: context.isLargeScreen
          ? null
          : Container(
              padding: const EdgeInsets.all(24),
              decoration: const BoxDecoration(
                color: Colors.white,
                boxShadow: [BoxShadow(color: Colors.grey, blurRadius: 2)],
                borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
              ),
              child: GradientButton(
                onPressed: () {},
                text: 'Kostenlos Registrieren',
              ),
            ),
    );
  }
}
