import 'package:flutter/material.dart';
import 'package:responsive_demo/ui/colors.dart';
import 'package:responsive_demo/ui/text_styles.dart';
import 'package:responsive_demo/ui/utils.dart';

class LandingPageHeader extends StatelessWidget {
  const LandingPageHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      'Deine Job\nwebsite',
      style: context.isLargeScreen
          ? 65.bold(color: AppColors.c2D3748)
          : 42.medium(color: AppColors.c2D3748),
      textAlign: context.isLargeScreen ? TextAlign.start : TextAlign.center,
    );
  }
}
