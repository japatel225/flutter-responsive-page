import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_demo/ui/pages/landing_page/landing_page_header.dart';
import 'package:responsive_demo/ui/pages/landing_page/tab_toggle.dart';
import 'package:responsive_demo/ui/widgets/bottom_wave_gradient_container.dart';
import 'package:responsive_demo/ui/widgets/gradient_button.dart';

class LandingPageBodyLarge extends StatefulWidget {
  const LandingPageBodyLarge({Key? key}) : super(key: key);

  @override
  State<LandingPageBodyLarge> createState() => _LandingPageBodyLargeState();
}

class _LandingPageBodyLargeState extends State<LandingPageBodyLarge> {
  int _selectedTabIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BottomWaveGradientContainer(
          child: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(width: 30),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const LandingPageHeader(),
                    const SizedBox(height: 48),
                    GradientButton(
                      onPressed: () {},
                      text: 'Kostenlos Registrieren',
                      width: 320,
                    )
                  ],
                ),
                const SizedBox(width: 140),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 30),
                  width: 455,
                  height: 455,
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(99999),
                  ),
                  child: SvgPicture.asset(
                    'assets/svgs/agreement_web.svg',
                    fit: BoxFit.fitWidth,
                  ),
                ),
                const SizedBox(width: 30),
              ],
            ),
          ),
        ),
        const SizedBox(height: 35),
        TabToggle(
          selectedIndex: _selectedTabIndex,
          onSelected: (i) => setState(() => _selectedTabIndex = i),
        ),
      ],
    );
  }
}
