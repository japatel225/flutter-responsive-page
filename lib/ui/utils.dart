import 'package:flutter/cupertino.dart';

extension Responsive on BuildContext {
  bool get isLargeScreen {
    final width = MediaQuery.of(this).size.width;
    return width > 500;
  }
}
