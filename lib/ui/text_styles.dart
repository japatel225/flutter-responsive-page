import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_demo/ui/colors.dart';

extension TextStyles on num {
  TextStyle medium({Color? color}) => GoogleFonts.lato(
        fontSize: toDouble(),
        color: color ?? AppColors.c718096,
        fontWeight: FontWeight.w500,
      );

  TextStyle semiBold({Color? color}) => GoogleFonts.lato(
        fontSize: toDouble(),
        color: color ?? AppColors.c718096,
        fontWeight: FontWeight.w600,
      );

  TextStyle bold({Color? color}) => GoogleFonts.lato(
        fontSize: toDouble(),
        color: color ?? AppColors.c718096,
        fontWeight: FontWeight.w700,
      );
}
