import 'dart:ui';

class AppColors {
  AppColors._();

  static const c718096 = Color(0xFF718096);
  static const c319795 = Color(0xFF319795);
  static const c3182CE = Color(0xFF3182CE);
  static const cE6FFFA = Color(0xFFE6FFFA);
  static const c2D3748 = Color(0xFF2D3748);
  static const cEBF4FF = Color(0xFFEBF4FF);
  static const c81E6D9 = Color(0xFF81E6D9);
  static const cCBD5E0 = Color(0xFFCBD5E0);
}
