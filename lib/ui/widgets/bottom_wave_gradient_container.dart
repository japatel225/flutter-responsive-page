import 'package:flutter/material.dart';
import 'package:responsive_demo/ui/colors.dart';

class BottomWaveGradientContainer extends StatelessWidget {
  final Widget child;

  const BottomWaveGradientContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: _BottomWaveClipper(),
      child: Container(
        padding: const EdgeInsets.only(bottom: 50),
        decoration: BoxDecoration(
          gradient: const LinearGradient(
            colors: [AppColors.cEBF4FF, AppColors.cE6FFFA],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
        child: child,
      ),
    );
  }
}

class _BottomWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final w = size.width;
    final h = size.height;
    const curveHeight = 70;

    final path = Path()
      ..moveTo(0, 0)
      ..lineTo(0, h - 20)
      ..quadraticBezierTo(40, h + 20, w / 2, h - (curveHeight / 2))
      ..quadraticBezierTo(
          w - 20, h - curveHeight - 20, w, h - (curveHeight / 2))
      ..lineTo(w, 0)
      ..close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper oldClipper) => true;
}
