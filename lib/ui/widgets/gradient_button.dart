import 'package:flutter/material.dart';
import 'package:responsive_demo/ui/colors.dart';
import 'package:responsive_demo/ui/text_styles.dart';

class GradientButton extends StatelessWidget {
  final double? width;
  final double height;
  final VoidCallback? onPressed;
  final String text;

  const GradientButton({
    Key? key,
    required this.onPressed,
    required this.text,
    this.width,
    this.height = 40.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          colors: [AppColors.c319795, AppColors.c3182CE],
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.transparent,
          elevation: 0,
          shadowColor: Colors.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
        ),
        child: Text(text, style: 14.semiBold(color: AppColors.cE6FFFA)),
      ),
    );
  }
}
